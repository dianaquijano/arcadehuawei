var app = angular.module("app", ["angularSpinner"]);

var config = {
  apiKey: "AIzaSyBuMdUCnKR6q9SLxsc0RdEKw9ReZilI1Bk",
  authDomain: "arcade-huawei.firebaseapp.com",
  databaseURL: "https://arcade-huawei.firebaseio.com",
  projectId: "arcade-huawei",
  storageBucket: "arcade-huawei.appspot.com",
  messagingSenderId: "370602322037"
};
firebase.initializeApp(config);

function obtenerScope() {
  return angular.element(document.body).scope();
}

function h9() {
  var $scope = obtenerScope("mainCtrl");
  $scope.gameOver = true;
  $scope.$apply();
}

/** c3 es la variable global que corresponde al juego */
var c4 = c3;

c4.setEndCallBack(window.h9);

app.run(function() {
  firebase
    .auth()
    .signInAnonymously()
    .catch(function(error) {
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
      // ...
    });
});

app.config([
  "usSpinnerConfigProvider",
  function(usSpinnerConfigProvider) {
    usSpinnerConfigProvider.setDefaults({ color: "red", size: 10 });
  }
]);

app.service("pageService", function($rootScope) {
  var srv = this;
  srv.pageSelected = "home";

  srv.selectPage = function(page) {
    srv.pageSelected = page;
    $rootScope.$broadcast("pageChanged", { page: page });
  };

  srv.checkPage = function(page) {
    return srv.pageSelected === page;
  };
});

app.service("usuarioService", function($rootScope) {
  var srv = this;
  srv.user = {};
  srv.refUsuarios = firebase.database().ref("/usuarios");
  srv.refScores = firebase.database().ref("/scores");

  /*SUBIR USUARIO*/
  srv.subirUsuario = function(nombre, correo, cedula) {
    srv.user.nombre = nombre;
    srv.user.correo = correo;
    srv.user.cedula = cedula;

    // /*
    //   * Register users in mixpanel
    // */
    // mixpanel.track("Users", {
    //   nombre: srv.user.nombre,
    //   correo: srv.user.correo,
    //   cedula: srv.user.cedula
    // });

    // mixpanel.people.set({
    //   $email: srv.user.correo,
    //   $last_login: new Date(),
    //   $name: srv.user.nombre
    // });

    // mixpanel.identify(srv.user.cedula);

    srv.refUsuarios
      .child("user" + cedula)
      .set({
        nombre: nombre,
        correo: correo,
        cedula: cedula,
        score: 0,
        fecha: String(new Date()),
        juegos: 0
      });
    srv.refScores
      .child("user" + cedula)
      .set({ score: 0, nombre: nombre, juegos: 0 });
  };

  srv.checkuser = function() {
    return srv.user && srv.user.cedula ? true : false;
  };
});

app.controller("puntajesController", function(
  pageService,
  $scope,
  usSpinnerService,
  usuarioService
) {
  var ictrl = this;

  ictrl.puntajes;
  ictrl.pageService = pageService;
  ictrl.usuarioService = usuarioService;

  /*traer puntaje*/

  ictrl.traerNuevos = function() {
    usSpinnerService.spin("spinner-1");
    firebase
      .database()
      .ref("usuarios")
      .orderByChild("score")
      .limitToLast(10)
      .once("value", function(snapshot) {
        usSpinnerService.stop("spinner-1");
        //console.log('snap:', snapshot.val());
        ictrl.puntajes = snapshot.val();
        ictrl.topScores = Object.keys(ictrl.puntajes).map(function(key) {
          return ictrl.puntajes[key];
        });
        apply();
      });
  };

  ictrl.cambioPaginaHandler = function(event, args) {
    if (args.page && args.page == "scores") {
      console.log("entrando a scores");
      //console.log(ictrl);
      ictrl.topScores = [];
      ictrl.traerNuevos();
    }
  };

  $scope.$on("pageChanged", ictrl.cambioPaginaHandler);

  var apply = function() {
    if (
      $scope.$root &&
      $scope.$root.$$phase != "$apply" &&
      $scope.$root.$$phase != "$digest"
    )
      $scope.$apply();
  };
});

app.controller("mainCtrl", function(
  $timeout,
  $scope,
  $window,
  pageService,
  usuarioService
) {
  var apply = function() {
    if (
      $scope.$root &&
      $scope.$root.$$phase != "$apply" &&
      $scope.$root.$$phase != "$digest"
    )
      $scope.$apply();
  };

  var ctrl = this;
  ctrl.pageService = pageService;
  ctrl.usuarioService = usuarioService;
  ctrl.newGame = true;
  ctrl.lapppedInit;
  ctrl.usuarioRef;

  ctrl.user = {};

  $scope.puntajeActual = 0;

  $scope.gameOver = false;

  $scope.$watch("gameOver", function(newval, oldvar) {
    if (newval === true) {
      ctrl.guardar_score();
    }
  });

  firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
      var isAnonymous = user.isAnonymous;
      var uid = user.uid;
    } else {
    }
  });

  ctrl.saveUser = function() {
    console.log("jugardar usuari", ctrl.newGame);
    ctrl.usuarioService.subirUsuario(
      ctrl.user.nombre,
      ctrl.user.mail,
      ctrl.user.cedula
    );

    if (ctrl.newGame) {
      console.log("nuevo");
      ctrl.startgame();
      ctrl.newGame = false;
    } else {
      document.getElementById("loading").style.display = "none";
      ctrl.pageService.selectPage("");
      ctrl.restartgame();
    }
    document.getElementById("registerForm").reset();
  };
  ctrl.guardar_score = function() {
    // manejar envio de datos con firebase
    firebase
      .database()
      .ref("/scores")
      .child("user" + ctrl.user.cedula)
      .once("value", function(snap) {
        //Actualizando la cantidad de juegos totales
        var juegostotales = Number(snap.val().juegos)
          ? Number(snap.val().juegos) + 1
          : 1;
        firebase
          .database()
          .ref("/scores")
          .child("user" + ctrl.user.cedula)
          .update({ juegos: juegostotales });
        firebase
          .database()
          .ref("/usuarios")
          .child("user" + ctrl.user.cedula)
          .update({ juegos: juegostotales });

        //Actualizando el tiempo que le tomo el juego
        var finalizado = new Date().getTime();
        var total = (finalizado - ctrl.lapppedInit) / 1000;
        ctrl.partidaRef.update({ endTime: finalizado, totalTime: total, score: c4.getScore()});

        total = (finalizado - ctrl.lapppedInit) / 1000;

        //Actualizando el puntaje
        var score =
          snap.val() && snap.val().score != null ? snap.val().score : 0;
        if (score < parseInt(c4.getScore())) {
          firebase
            .database()
            .ref("/usuarios")
            .child("user" + ctrl.user.cedula)
            .update({
              score: parseInt(c4.getScore()),
              fecha: String(new Date()),
              tiempoTotal: total
            });
          firebase
            .database()
            .ref("/scores")
            .child("user" + ctrl.user.cedula)
            .update({
              score: parseInt(c4.getScore()),
              fecha: String(new Date()),
              tiempoTotal: total
            });
        }

        $scope.puntajeActual = parseInt(c4.getScore());
        ctrl.pageService.selectPage("gameOver");
        $timeout(function() {
          ctrl.pageService.selectPage("scores");
        }, 3000);

        document.getElementById("loading").style.display = "flex";
        apply();
      });

    $scope.gameOver = false;
  };

  function getCook(cookiename) {
    // Get name followed by anything except a semicolon
    var cookiestring = RegExp("" + cookiename + "[^;]+").exec(document.cookie);
    // Return everything after the equal sign, or an empty string if the cookie name not found
    return unescape(
      !!cookiestring ? cookiestring.toString().replace(/^[^=]+./, "") : ""
    );
  }

  //Sample usage
  /*Logica login*/
  ctrl.login = function() {
    srv = this;
    // console.log(ctrl.user.cedula);

    // /*
    //   * This tracks users activity 
    // */
    // mixpanel.track('Play game', {
    //   user: srv.user.cedula,
    //   nombre: srv.user.nombre
    // }
    // );

    // mixpanel.people.set({
    //   $last_login: new Date()
    // });

    // mixpanel.identify(srv.user.cedula);

    // /*
    // * Here ends mixpanel
    // */
    
    firebase
      .database()
      .ref("usuarios")
      .child("user" + ctrl.user.cedula)
      .once("value", function(snap) {
        ctrl.usuarioService.user = snap.val();
        ctrl.user = snap.val();

        if (snap.val() != null) {
          if (ctrl.newGame) {
            var cookieValue = getCook("instructions");
            if (cookieValue) {
              ctrl.startgame();
              ctrl.newGame = false;
            } else {
              document.cookie = "instructions=true";
              ctrl.pageService.selectPage("instructions");
            }
          } else {
            ctrl.restartgame();
          }
        } else {
          ctrl.pageService.selectPage("form");
        }
        apply();
      });
        document.getElementById("loginForm").reset();
  };

  ctrl.instructions = function() {
    c4.init();
    ctrl.newGame = false;
    console.log("go to game");
  };

  ctrl.prestartgame = function() {
    var usuarioRef = firebase
      .database()
      .ref("/usuarios/user" + ctrl.user.cedula);
    ctrl.lapppedInit = new Date().getTime();
    ctrl.partidaRef = usuarioRef
      .child("partidas")
      .push({ startTime: ctrl.lapppedInit });
  };

  ctrl.startgame = function() {
    ctrl.prestartgame();
    document.getElementById("loading").style.display = "none";
    //  ctrl.pageService.selectPage('');
    c4.init();
  };
  ctrl.restartgame = function() {
    ctrl.prestartgame();
    document.getElementById("loading").style.display = "none";
    ctrl.pageService.selectPage("");
    c4.restart();
  };

  $scope.isActive = false;
  ctrl.mute = function(event) {
    /*
    $scope.isActive = !$scope.isActive;
    if (c4.backgroundAudio.volume === 0.25) {
      c4.backgroundAudio.volume = 0;
      c4.gameOverAudio.volume = 0;
    } else {
      c4.backgroundAudio.volume = 0.25;
      c4.gameOverAudio.volume = 0.25;
    }
    */
  };
});
