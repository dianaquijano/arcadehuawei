// JavaScript Model
var model = {
    boardSize: 7,
    numShips: 3,
    shipLength: 3,
    shipsSunk: 0,

	ships: [
		{ locations: [0, 0, 0], hits: ["", "", ""] },
		{ locations: [0, 0, 0], hits: ["", "", ""] },
		{ locations: [0, 0, 0], hits: ["", "", ""] }
	],

	fire: function(guess) {

		for(var i = 0; i < this.numShips; i++) {
			var ship = this.ships[i];
			var index = ship.locations.indexOf(guess);

			// check if a ship location has already been hit
			if ( ship.hits[index] === "hit" ) {
				view.displayMessage("Oops, you already hit that location");
				return true;
			} else if ( index >= 0 ) {
				ship.hits[index] = "hit";
				view.displayHit(guess);
				view.displayMessage("HIT!");

				if ( this.isSunk(ship) ) {
					view.displayMessage("You sank my battleship!");
					this.shipsSunk++;
				}
				return true;
			}
			$('#guessInput').focus();
		}
		view.displayMiss(guess);
		view.displayMessage("You Missed");
		return false;
	},

	isSunk: function(ship) {
		for (var i = 0; i < this.shipLength; i++) {
			if (ship.hits[i] !== "hit") {
				return false;
			}
		}
		$('#guessInput').focus();
		return true;
	},

	generateShipLocations: function() {
		var locations;
		for (var i = 0; i < this.numShips; i++) {
		do {
				locations = this.generateShip();
		} while (this.collision(locations));
			this.ships[i].locations = locations;
		}
	},

	generateShip: function() {
		var direction = Math.floor(Math.random() * 2);
		var row, col;

		if (direction === 1) { // horizontal
			row = Math.floor(Math.random() * this.boardSize);
			col = Math.floor(Math.random() * (this.boardSize - this.shipLength + 1));
		} else { // vertical
			row = Math.floor(Math.random() * (this.boardSize - this.shipLength + 1));
			col = Math.floor(Math.random() * this.boardSize);
		}

		var newShipLocations = [];

		for (var i = 0; i < this.shipLength; i++) {
			if (direction === 1) {
				newShipLocations.push(row + "" + (col + i));
			} else {
				newShipLocations.push((row + i) + "" + col);
			}
		}
		return newShipLocations;
	},

	collision: function(locations) {
		for (var i = 0; i < this.numShips; i++) {
			var ship = this.ships[i];
			for (var j = 0; j < locations.length; j++) {
				if (ship.locations.indexOf(locations[j]) >= 0) {
					return true;
				}
			}
		}
		return false;
	}
};

var view = {
	displayMessage: function(msg) {
		var messageArea = document.getElementById("messageArea");
		messageArea.innerHTML = msg;
	},
	displayHit: function(location) {
		var cell = document.getElementById(location);
		cell.setAttribute("class", "hit");
	},
	displayMiss: function(location) {
		var cell = document.getElementById(location);
		cell.setAttribute("class", "miss");
	}
};

var controller = {
	guesses: 0,

	processGuess: function(guess) {
		var location = parseGuess(guess);

		if (location) {
			this.guesses++;
			var hit = model.fire(location);
			if (hit && model.shipsSunk === model.numShips) {
				view.displayMessage("You sank all my battleships, in " + this.guesses + " guesses");
			}
		}
	}
};

// helper function to parse a guess from the user
function parseGuess(guess) {
	var alphabet = ["A", "B", "C", "D", "E", "F", "G"];

	if (guess === null || guess.length !== 2) {
		alert("Oops, please enter a letter and a number on the board.");
	} else {
		var firstChar = guess.charAt(0);
		var row = alphabet.indexOf(firstChar);
		var column = guess.charAt(1);
		if (isNaN(row) || isNaN(column)) {
			alert("Oops, that isn't on the board.");
		} else if (row < 0 || row >= model.boardSize || column < 0 || column >= model.boardSize) {
				alert("Oops, that's off the board!");
		} else {
			return row + column;
		}
	}
	return null;
}

// event handlers
function handleFireButton() {
	var guessInput = document.getElementById("guessInput");
	var guess = guessInput.value.toUpperCase();
	controller.processGuess(guess);
	guessInput.value = "";
}

function handleKeyPress(e) {
	var fireButton = document.getElementById("fireButton");
	// in IE9 and earlier, the event object doesn't get passed
	// to the event handler correctly, so we use window.event instead.
	e = e || window.event;
	if (e.keyCode === 13) {
		fireButton.click();
		return false;
	}
}


var c3 = (function () {

	/**
	 * Define an object to hold all our images for the game so images
	 * are only ever created once. This type of object is known as a
	 * singleton.
	 */
	
	var imageRepository = new function() {
		
		
		// Define images
		this.background = new Image();
		this.spaceship = new Image();
		this.bullet = new Image();
		this.enemy = new Image();
		this.enemyBullet = new Image();
	
		// Ensure all images have loaded before starting the game
		var numImages = 5;
		var numLoaded = 0;
		function imageLoaded() {
			numLoaded++;
			if (numLoaded === numImages) {
				//init();
			}
		}
		this.background.onload = function() {
			imageLoaded();
		};
		this.spaceship.onload = function() {
			imageLoaded();
		};
		this.bullet.onload = function() {
			imageLoaded();
		};
		this.enemy.onload = function() {
			imageLoaded();
		};
		this.enemyBullet.onload = function() {
			imageLoaded();
		};
	
		// Set images src
		this.background.src = "imgs/bg.png";
		this.spaceship.src = "imgs/ship.png";
		this.bullet.src = "imgs/bullet.png";
		this.enemy.src = "imgs/enemy.png";
		this.enemyBullet.src = "imgs/bullet_enemy.png";
	};
	
	
	/**
	 * Creates the Drawable object which will be the base class for
	 * all drawable objects in the game. Sets up defualt variables
	 * that all child objects will inherit, as well as the defualt
	 * functions.
	 */
	function Drawable() {
		this.init = function(x, y, width, height) {
			// Defualt variables
			this.x = x;
			this.y = y;
			this.width = width;
			this.height = height;
			this.isColliding = false;
		}
	
		this.speed = 0;
		this.canvasWidth = 0;
		this.canvasHeight = 0;
		this.collidableWith = "";
		this.isColliding = false;
		this.type = "";
	
		// Define abstract function to be implemented in child objects
		this.draw = function() {
		};
		this.move = function() {
		};
		this.isCollidableWith = function(object) {
			return (this.collidableWith === object.type);
		};
	}
	
	
	/**
	 * Creates the Background object which will become a child of
	 * the Drawable object. The background is drawn on the "background"
	 * canvas and creates the illusion of moving by panning the image.
	 */
	function Background() {
		this.speed = 1; // Redefine speed of the background for panning
	
		// Implement abstract function
		this.draw = function() {
			// Pan background
			this.y += this.speed;
			//this.context.clearRect(0,0, this.canvasWidth, this.canvasHeight);
			this.context.drawImage(imageRepository.background, this.x, this.y);
	
			// Draw another image at the top edge of the first image
			this.context.drawImage(imageRepository.background, this.x, this.y - this.canvasHeight);
	
			// If the image scrolled off the screen, reset
			if (this.y >= this.canvasHeight)
				this.y = 0;
		};
	}
	// Set Background to inherit properties from Drawable
	Background.prototype = new Drawable();
	
	
	function Limit() {
		this.speed = 0; 
		this.isColliding = false;
		this.collidableWith = "enemy";
		this.type = "limit";
		// Implement abstract function
		this.draw = function() {
			if (this.isColliding){
				
				
				game.ship.alive = false;
				game.gameOver();
			}
		};
	}
	// Set Background to inherit properties from Drawable
	Limit.prototype = new Drawable();
	
	/**
	 * Creates the Bullet object which the ship fires. The bullets are
	 * drawn on the "main" canvas.
	 */
	function Bullet(object) {
		this.alive = false; // Is true if the bullet is currently in use
		var self = object;
		/*
		 * Sets the bullet values
		 */
		this.spawn = function(x, y, speed) {
			this.x = x;
			this.y = y;
			this.speed = speed;
			this.alive = true;
		};
	
		/*
		 * Uses a "drity rectangle" to erase the bullet and moves it.
		 * Returns true if the bullet moved of the screen, indicating that
		 * the bullet is ready to be cleared by the pool, otherwise draws
		 * the bullet.
		 */
		this.draw = function() {
			this.context.clearRect(this.x-1, this.y-1, this.width+2, this.height+2);
			this.y -= this.speed;
	
			if (this.isColliding) {
				return true;
			}
			else if (self === "bullet" && this.y <= 0 - this.height) {
				return true;
			}
			else if (self === "enemyBullet" && this.y >= this.canvasHeight) {
				return true;
			}
			else {
				if (self === "bullet") {
					this.context.drawImage(imageRepository.bullet, this.x, this.y);
				}
				else if (self === "enemyBullet") {
					this.context.drawImage(imageRepository.enemyBullet, this.x, this.y);
				}
	
				return false;
			}
		};
	
		/*
		 * Resets the bullet values
		 */
		this.clear = function() {
			this.x = 0;
			this.y = 0;
			this.speed = 0;
			this.alive = false;
			this.isColliding = false;
		};
	}
	Bullet.prototype = new Drawable();
	
	
	/**
	 * QuadTree object.
	 *
	 * The quadrant indexes are numbered as below:
	 *     |
	 *  1  |  0
	 * ----+----
	 *  2  |  3
	 *     |
	 */
	function QuadTree(boundBox, lvl) {
		var maxObjects = 10;
		this.bounds = boundBox || {
			x: 0,
			y: 0,
			width: 0,
			height: 0
		};
		var objects = [];
		this.nodes = [];
		var level = lvl || 0;
		var maxLevels = 5;
	
		/*
		 * Clears the quadTree and all nodes of objects
		 */
		this.clear = function() {
			objects = [];
	
			for (var i = 0; i < this.nodes.length; i++) {
				this.nodes[i].clear();
			}
	
			this.nodes = [];
		};
	
		/*
		 * Get all objects in the quadTree
		 */
		this.getAllObjects = function(returnedObjects) {
			for (var i = 0; i < this.nodes.length; i++) {
				this.nodes[i].getAllObjects(returnedObjects);
			}
	
			for (var i = 0, len = objects.length; i < len; i++) {
				returnedObjects.push(objects[i]);
			}
	
			return returnedObjects;
		};
	
		/*
		 * Return all objects that the object could collide with
		 */
		this.findObjects = function(returnedObjects, obj) {
			if (typeof obj === "undefined") {
		
				return;
			}
	
			var index = this.getIndex(obj);
			if (index != -1 && this.nodes.length) {
				this.nodes[index].findObjects(returnedObjects, obj);
			}
	
			for (var i = 0, len = objects.length; i < len; i++) {
				returnedObjects.push(objects[i]);
			}
	
			return returnedObjects;
		};
	
		/*
		 * Insert the object into the quadTree. If the tree
		 * excedes the capacity, it will split and add all
		 * objects to their corresponding nodes.
		 */
		this.insert = function(obj) {
			if (typeof obj === "undefined") {
				return;
			}
	
			if (obj instanceof Array) {
				for (var i = 0, len = obj.length; i < len; i++) {
					this.insert(obj[i]);
				}
	
				return;
			}
	
			if (this.nodes.length) {
				var index = this.getIndex(obj);
				// Only add the object to a subnode if it can fit completely
				// within one
				if (index != -1) {
					this.nodes[index].insert(obj);
	
					return;
				}
			}
	
			objects.push(obj);
	
			// Prevent infinite splitting
			if (objects.length > maxObjects && level < maxLevels) {
				if (this.nodes[0] == null) {
					this.split();
				}
	
				var i = 0;
				while (i < objects.length) {
	
					var index = this.getIndex(objects[i]);
					if (index != -1) {
						this.nodes[index].insert((objects.splice(i,1))[0]);
					}
					else {
						i++;
					}
				}
			}
		};
	
		/*
		 * Determine which node the object belongs to. -1 means
		 * object cannot completely fit within a node and is part
		 * of the current node
		 */
		this.getIndex = function(obj) {
	
			var index = -1;
			var verticalMidpoint = this.bounds.x + this.bounds.width / 2;
			var horizontalMidpoint = this.bounds.y + this.bounds.height / 2;
	
			// Object can fit completely within the top quadrant
			var topQuadrant = (obj.y < horizontalMidpoint && obj.y + obj.height < horizontalMidpoint);
			// Object can fit completely within the bottom quandrant
			var bottomQuadrant = (obj.y > horizontalMidpoint);
	
			// Object can fit completely within the left quadrants
			if (obj.x < verticalMidpoint &&
					obj.x + obj.width < verticalMidpoint) {
				if (topQuadrant) {
					index = 1;
				}
				else if (bottomQuadrant) {
					index = 2;
				}
			}
			// Object can fix completely within the right quandrants
			else if (obj.x > verticalMidpoint) {
				if (topQuadrant) {
					index = 0;
				}
				else if (bottomQuadrant) {
					index = 3;
				}
			}
	
			return index;
		};
	
		/*
		 * Splits the node into 4 subnodes
		 */
		this.split = function() {
			// Bitwise or [html5rocks]
			var subWidth = (this.bounds.width / 2) | 0;
			var subHeight = (this.bounds.height / 2) | 0;
	
			this.nodes[0] = new QuadTree({
				x: this.bounds.x + subWidth,
				y: this.bounds.y,
				width: subWidth,
				height: subHeight
			}, level+1);
			this.nodes[1] = new QuadTree({
				x: this.bounds.x,
				y: this.bounds.y,
				width: subWidth,
				height: subHeight
			}, level+1);
			this.nodes[2] = new QuadTree({
				x: this.bounds.x,
				y: this.bounds.y + subHeight,
				width: subWidth,
				height: subHeight
			}, level+1);
			this.nodes[3] = new QuadTree({
				x: this.bounds.x + subWidth,
				y: this.bounds.y + subHeight,
				width: subWidth,
				height: subHeight
			}, level+1);
		};
	}
	
	
	/**
	 * Custom Pool object. Holds Bullet objects to be managed to prevent
	 * garbage collection.
	 * The pool works as follows:
	 * - When the pool is initialized, it popoulates an array with
	 *   Bullet objects.
	 * - When the pool needs to create a new object for use, it looks at
	 *   the last item in the array and checks to see if it is currently
	 *   in use or not. If it is in use, the pool is full. If it is
	 *   not in use, the pool "spawns" the last item in the array and
	 *   then pops it from the end and pushed it back onto the front of
	 *   the array. This makes the pool have free objects on the back
	 *   and used objects in the front.
	 * - When the pool animates its objects, it checks to see if the
	 *   object is in use (no need to draw unused objects) and if it is,
	 *   draws it. If the draw() function returns true, the object is
	 *   ready to be cleaned so it "clears" the object and uses the
	 *   array function splice() to remove the item from the array and
	 *   pushes it to the back.
	 * Doing this makes creating/destroying objects in the pool
	 * constant.
	 */
	function Pool(maxSize) {
		var size = maxSize; // Max bullets allowed in the pool
		var pool = [];
	
		this.getPool = function() {
			var obj = [];
			for (var i = 0; i < size; i++) {
				if (pool[i].alive) {
					obj.push(pool[i]);
				}
			}
			return obj;
		}
	
		/*
		 * Populates the pool array with the given object
		 */
		this.init = function(object) {
			if (object == "bullet") {
				for (var i = 0; i < size; i++) {
					// Initalize the object
					var bullet = new Bullet("bullet");
					bullet.init(0,0, imageRepository.bullet.width,
											imageRepository.bullet.height);
					bullet.collidableWith = "enemy";
					bullet.type = "bullet";
					pool[i] = bullet;
				}
			}
			else if (object == "enemy") {
				for (var i = 0; i < size; i++) {
					var enemy = new Enemy();
					enemy.init(0,0, imageRepository.enemy.width,
										 imageRepository.enemy.height);
					pool[i] = enemy;
				}
			}
			else if (object == "enemyBullet") {
				for (var i = 0; i < size; i++) {
					var bullet = new Bullet("enemyBullet");
					bullet.init(0,0, imageRepository.enemyBullet.width,
											imageRepository.enemyBullet.height);
					bullet.collidableWith = "ship";
					bullet.type = "enemyBullet";
					pool[i] = bullet;
				}
			}
		};
	
		/*
		 * Grabs the last item in the list and initializes it and
		 * pushes it to the front of the array.
		 */
		this.get = function(x, y, speed) {
			if(!pool[size - 1].alive) {
				pool[size - 1].spawn(x, y, speed);
				pool.unshift(pool.pop());
			}
		};
	
		/*
		 * Used for the ship to be able to get two bullets at once. If
		 * only the get() function is used twice, the ship is able to
		 * fire and only have 1 bullet spawn instead of 2.
		 */
		this.getTwo = function(x1, y1, speed1, x2, y2, speed2) {
			if(!pool[size - 1].alive && !pool[size - 2].alive) {
				this.get(x1, y1, speed1);
				this.get(x2, y2, speed2);
			}
		};
	
		/*
		 * Draws any in use Bullets. If a bullet goes off the screen,
		 * clears it and pushes it to the front of the array.
		 */
		this.animate = function() {
			for (var i = 0; i < size; i++) {
				// Only draw until we find a bullet that is not alive
				if (pool[i].alive) {
					if (pool[i].draw()) {
						pool[i].clear();
						pool.push((pool.splice(i,1))[0]);
					}
				}
				else
					break;
			}
		};
	}
	
	
	/**
	 * Create the Ship object that the player controls. The ship is
	 * drawn on the "ship" canvas and uses dirty rectangles to move
	 * around the screen.
	 */
	function Ship() {
		this.speed = 3;
		this.bulletPool = new Pool(30);
		var fireRate = 40;
		var counter = 0;
		this.collidableWith = "enemyBullet";
		this.type = "ship";
	
		this.init = function(x, y, width, height) {
			// Defualt variables
			this.x = x;
			this.y = y;
			this.width = width;
			this.height = height;
			this.alive = true;
			this.isColliding = false;
			this.bulletPool.init("bullet");
		}
	
		this.draw = function() {
			this.context.drawImage(imageRepository.spaceship, this.x, this.y);
		};
		this.move = function() {
			counter++;
			// Determine if the action is move action
			if (KEY_STATUS.left || KEY_STATUS.right ||
					KEY_STATUS.down || KEY_STATUS.up) {
				// The ship moved, so erase it's current image so it can
				// be redrawn in it's new location
				this.context.clearRect(this.x, this.y, this.width, this.height);
	
				// Update x and y according to the direction to move and
				// redraw the ship. Change the else if's to if statements
				// to have diagonal movement.
				var foo = document.getElementsByClassName('joystick');
				if (KEY_STATUS.left) {
					foo[0].classList.remove('right')
					foo[0].classList.add('left')
					this.x -= this.speed
					if (this.x <= 35) // Kep player within the screen
						this.x = 35;
				} else if (KEY_STATUS.right) {
					foo[0].classList.remove('left')
					foo[0].classList.add('right')
					this.x += this.speed
					if (this.x >= this.canvasWidth - (this.width + 35))
						this.x = this.canvasWidth - (this.width + 35);
				}
			}
	
			// Redraw the ship
			if (!this.isColliding) {
				this.draw();
			}
			else {
				
				this.alive = false;
				game.gameOver();
			}
			var btn = document.getElementsByClassName('button_machine');
			if (KEY_STATUS.space && counter >= fireRate && !this.isColliding) {
			
				this.fire();
				counter = 0;
	/*			btn[0].classList.add('abajo');
				setTimeout(function() {
					btn[0].classList.remove('abajo');
				}, 300);*/
			}
		};
	
		/*
		 * Fires two bullets
		 */
		this.fire = function() {
			this.bulletPool.getTwo(this.x+6, this.y, 3, this.x + 33, this.y, 3);
			game.laser.get();
		};
	}
	Ship.prototype = new Drawable();
	
	
	/**
	 * Create the Enemy ship object.
	 */
	function Enemy() {
		var percentFireBase = 0.003;
		var percentFire = 0.003;
		var chance = 0;
		this.alive = false;
		this.collidableWith = "bullet";
		this.type = "enemy";
		this.choquePared = 0;
		this.test = true;
	
		/*
		 * Sets the Enemy values
		 */
		this.spawn = function(x, y, speed) {
			
			
			percentFire    =  percentFireBase + (speed/700);
			this.counter   = this.counter + 1 || 1;
			this.downShips = this.downShips + this.y || this.y; //
	
			this.posicionadoInicialmente = false;
			this.x = x;
			this.y = y;
			this.speed = speed;
			this.speedX = 0;
			this.speedY = speed;
			this.alive = true;
			this.leftEdge = this.x - 90;
			this.rightEdge = this.x + 90;
			this.bottomEdge = this.y + 140; //this.bottomEdge = this.y + (140 + (this.counter * this.height));
			this.powerShip = this.powerShip / 1.75 || 300; //
		
		};
		
		
	
	
		/*
		 * Move the enemy
		 */
	
		this.draw = function() {
			
			this.context.clearRect(this.x-1, this.y, this.width+1, this.height);
			this.y += this.speedY;
			this.x += this.speedX;
	
	
			/* Condiciones limite */
			
			//estrella pared izquierda
			if (this.x <= this.leftEdge) {
				this.speedX = this.speed;
				this.y += 10;
			}
			//estrella paread derecha
			else if (this.x >= this.rightEdge + this.width) {
				this.speedX = -this.speed;
				
			}
			
			
	
			//posicionamiento inicial del enemigo que cae de arriba de la pantalla hasta su posicion
			else if (!this.posicionadoInicialmente && this.y > this.bottomEdge) {
				this.posicionadoInicialmente = true;
				this.speed = this.speed; //this.speed = 1.5;//original 1.5
				this.speedY = 0;
				this.y -= 5;
				this.speedX = -this.speed;
			}		
			
	
	
			//Revisamos si mataron este enemigo
			if (!this.isColliding) { 
				this.context.drawImage(imageRepository.enemy, this.x, this.y);
				
				
				chance = Math.random();
				// Enemy has a chance to shoot every movement
				if (chance < percentFire) {			
					this.fire();
				}				
				return false;
			}
			else {
				game.playerScore += 10; 
				game.explosion.get();
				return true;
			}
			
			
			
			
		};
	
		/*
		 * Fires a bullet
		 */
		this.fire = function() {
			game.enemyBulletPool.get(this.x+this.width/2, this.y+this.height, -2.5);
		};
	
		/*
		 * Resets the enemy values
		 */
		this.clear = function() {
			this.x = 0;
			this.y = 0;
			this.speed = 0;
			this.speedX = 0;
			this.speedY = 0;
			this.alive = false;
			this.isColliding = false;
		};
	}
	Enemy.prototype = new Drawable();
	
	
	 /**
	 * Creates the Game object which will hold all objects and data for
	 * the game.
	 */
	function Game() {
		
		var self = this;
		self.EndCallBack = null;
		self.papas = false;
		/* Referencia al nivel del juego a medida que el jugador va avanzando */
		self.nivel = 0;
		self.levelUp = function(){
			self.nivel++;
		};
		/*
		 * Gets canvas information and context and sets up all game
		 * objects.
		 * Returns true if the canvas is supported and false if it
		 * is not. This is to stop the animation script from constantly
		 * running on browsers that do not support the canvas.
		 */
		self.init = function() {
			
			self.papas = false;
			// Get the canvas elements
			self.bgCanvas = document.getElementById('background');
			self.shipCanvas = document.getElementById('ship');
			self.mainCanvas = document.getElementById('main');
	
			// Test to see if canvas is supported. Only need to
			// check one canvas
			if (self.bgCanvas.getContext) {
				self.bgContext = self.bgCanvas.getContext('2d');
				self.shipContext = self.shipCanvas.getContext('2d');
				self.mainContext = self.mainCanvas.getContext('2d');
	
				// Initialize objects to contain their context and canvas
				// information
				Background.prototype.context = self.bgContext;
				Background.prototype.canvasWidth = self.bgCanvas.width;
				Background.prototype.canvasHeight = self.bgCanvas.height;
	
				Ship.prototype.context = self.shipContext;
				Ship.prototype.canvasWidth = self.shipCanvas.width;
				Ship.prototype.canvasHeight = self.shipCanvas.height;
	
				Bullet.prototype.context = self.mainContext;
				Bullet.prototype.canvasWidth = self.mainCanvas.width;
				Bullet.prototype.canvasHeight = self.mainCanvas.height;
	
				Enemy.prototype.context = self.mainContext;
				Enemy.prototype.canvasWidth = self.mainCanvas.width;
				Enemy.prototype.canvasHeight = self.mainCanvas.height;
				
				Limit.prototype.context = self.mainContext;
				Limit.prototype.canvasWidth = self.mainCanvas.width;
				Limit.prototype.canvasHeight = self.mainCanvas.height;			
				
				
				
				// Initialize the background object
				self.background = new Background();
				self.background.init(0,0); // Set draw point to 0,0
	
				// Initialize the ship object
				self.ship = new Ship();
				// Set the ship to start near the bottom middle of the canvas
				self.shipStartX = self.shipCanvas.width/2 - imageRepository.spaceship.width;
				self.shipStartY = self.shipCanvas.height/4*3 + imageRepository.spaceship.height*2;
				self.ship.init(self.shipStartX, self.shipStartY,
							   imageRepository.spaceship.width, imageRepository.spaceship.height);
	
				self.limiteInferior = new Limit();
				self.limiteInferior.init(0,self.mainCanvas.height-50,self.mainCanvas.width,1);  
				  
			
				// Initialize the enemy pool object
				self.enemyPool = new Pool(30);
				self.enemyPool.init("enemy");
				self.spawnWave();
	
				self.enemyBulletPool = new Pool(50);
				self.enemyBulletPool.init("enemyBullet");
	
				// Start QuadTree
				self.quadTree = new QuadTree({x:0,y:0,width:self.mainCanvas.width,height:self.mainCanvas.height});
	
				self.playerScore = 0;
	
				// Audio files
				self.laser = new SoundPool(10);
				self.laser.init("laser");
	
				self.explosion = new SoundPool(20);
				self.explosion.init("explosion");
	
				/*
				self.backgroundAudio = new Audio("sounds/kick_shock.mp3");
				self.backgroundAudio.loop = true;
				self.backgroundAudio.volume = .25;
				self.backgroundAudio.load();
				*/
				/*
				self.gameOverAudio = new Audio("sounds/game_over.mp3");
				self.gameOverAudio.loop = true;
				self.gameOverAudio.volume = .25;
				self.gameOverAudio.load();
				*/
	
				self.checkAudio = window.setInterval(function(){self.checkReadyState()},1000);
			}
		};
	/**
	 * Ensure the game sound has loaded before starting the game
	 */	
		self.checkReadyState = function() {
		//if (self.gameOverAudio.readyState === 4) {
			window.clearInterval(self.checkAudio);
			document.getElementById('loading').style.display = "none";
			self.start();
		//}
	}
	
	
		// Spawn a new wave of enemies
		self.spawnWave = function() {
			var height = imageRepository.enemy.height;
			var width  = imageRepository.enemy.width;
			var x = 100;
			var y = -height;
			var spacer = y * 1.5;
			var velocidad = self.nivel*0.5 + 1.4;//original 1.5
			for (var i = 1; i <= 18; i++) {
				self.enemyPool.get(x,y,velocidad);
				x += width + 25;
				if (i % 6 == 0) {
					x = 100;
					y += spacer
				}
			}
		}
	
		// Start the animation loop
		self.start = function() {
			self.ship.draw();
			//self.backgroundAudio.play();
			animate();
		};
	
		// Restart the game
		self.restart = function() {
			self.papas = false;
			//self.gameOverAudio.pause();
			self.nivel = 0;
			document.getElementById('game-over').style.display = "none";
			self.bgContext.clearRect(0, 0, self.bgCanvas.width, self.bgCanvas.height);
			self.shipContext.clearRect(0, 0, self.shipCanvas.width, self.shipCanvas.height);
			self.mainContext.clearRect(0, 0, self.mainCanvas.width, self.mainCanvas.height);
		
			self.quadTree.clear();
	
			self.background.init(0,0);
			self.ship.init(self.shipStartX, self.shipStartY,
						   imageRepository.spaceship.width, imageRepository.spaceship.height);
						   
			 self.limiteInferior.init(0,self.mainCanvas.height-50,self.mainCanvas.width,1);                 
	
			self.enemyPool.init("enemy");
			self.spawnWave();
			self.enemyBulletPool.init("enemyBullet");
	
			self.playerScore = 0;
	
			//self.backgroundAudio.currentTime = 0;
			//self.backgroundAudio.play();
	
			self.start();
		};
	
		// Game over   
		self.gameOver = function() {		
			//self.backgroundAudio.pause();
			//self.gameOverAudio.currentTime = 0;
			//self.gameOverAudio.play();
	
			self.papas = true;
			if (self.EndCallBack)
			self.EndCallBack();
	
		};
		
		self.getScore = function(){
			return self.playerScore;
		}
		
		self.setEndCallBack = function(callBack){
			self.EndCallBack = callBack;
		};
	}
	
	
	
	/**
	 * A sound pool to use for the sound effects
	 */
	function SoundPool(maxSize) {
		var size = maxSize; // Max bullets allowed in the pool
		var pool = [];
		this.pool = pool;
		var currSound = 0;
	
		/*
		 * Populates the pool array with the given object
		 */
		this.init = function(object) {
			if (object == "laser") {
				for (var i = 0; i < size; i++) {
					// Initalize the object
					laser = new Audio("sounds/laser.wav");
					laser.volume = .12;
					laser.load();
					pool[i] = laser;
				}
			}
			else if (object == "explosion") {
				for (var i = 0; i < size; i++) {
					var explosion = new Audio("sounds/explosion.wav");
					explosion.volume = .1;
					explosion.load();
					pool[i] = explosion;
				}
			}
		};
	
		/*
		 * Plays a sound
		 */
		this.get = function() {
		
			if(pool[currSound].currentTime == 0 || pool[currSound].ended) {
				pool[currSound].play();
			}
			currSound = (currSound + 1) % size;
		};
	}
	
	
	/**
	 * The animation loop. Calls the requestAnimationFrame shim to
	 * optimize the game loop and draws all game objects. This
	 * function must be a gobal function and cannot be within an
	 * object.
	 */
	function animate() {
		document.getElementById('score').innerHTML = game.playerScore;
	
		// Insert objects into quadtree
		game.quadTree.clear();
		game.quadTree.insert(game.ship);
		game.quadTree.insert(game.limiteInferior);
		game.quadTree.insert(game.ship.bulletPool.getPool());
		game.quadTree.insert(game.enemyPool.getPool());
		game.quadTree.insert(game.enemyBulletPool.getPool());
	
		detectCollision();
	
		// No more enemies
		if (game.enemyPool.getPool().length === 0) {
			game.levelUp();
			game.spawnWave();
		}
	
		// Animate game objects
		if (game.ship.alive) {
			requestAnimFrame( animate );
	
			game.background.draw();
			game.limiteInferior.draw();
			game.ship.move();
			game.ship.bulletPool.animate();
			game.enemyPool.animate();
			game.enemyBulletPool.animate();
		}
	}
	
	function detectCollision() {
		var objects = [];
		game.quadTree.getAllObjects(objects);
	
		for (var x = 0, len = objects.length; x < len; x++) {
			game.quadTree.findObjects(obj = [], objects[x]);
	
			for (y = 0, length = obj.length; y < length; y++) {
	
				// DETECT COLLISION ALGORITHM
				if (objects[x].collidableWith === obj[y].type &&
					(objects[x].x < obj[y].x + obj[y].width &&
					 objects[x].x + objects[x].width > obj[y].x &&
					 objects[x].y < obj[y].y + obj[y].height &&
					 objects[x].y + objects[x].height > obj[y].y)) {
					objects[x].isColliding = true;
					obj[y].isColliding = true;
				}
			}
		}
	};
	
	
	// The keycodes that will be mapped when a user presses a button.
	// Original code by Doug McInnes
	KEY_CODES = {
	  32: 'space',
	  37: 'left',
	  38: 'up',
	  39: 'right',
	  40: 'down'
	};
	
	// Creates the array to hold the KEY_CODES and sets all their values
	// to true. Checking true/flase is the quickest way to check status
	// of a key press and which one was pressed when determining
	// when to move and which direction.
	KEY_STATUS = {};
	for (code in KEY_CODES) {
	  KEY_STATUS[KEY_CODES[code]] = false;
	}
	/**
	 * Sets up the document to listen to onkeydown events (fired when
	 * any key on the keyboard is pressed down). When a key is pressed,
	 * it sets the appropriate direction to true to let us know which
	 * key it was.
	 */
	document.onkeydown = function(e) {
		// Firefox and opera use charCode instead of keyCode to
		// return which key was pressed.
		var keyCode = (e.keyCode) ? e.keyCode : e.charCode;
	  if (KEY_CODES[keyCode]) {
			e.preventDefault();
		KEY_STATUS[KEY_CODES[keyCode]] = true;
	  }
	}
	/**
	 * Sets up the document to listen to ownkeyup events (fired when
	 * any key on the keyboard is released). When a key is released,
	 * it sets teh appropriate direction to false to let us know which
	 * key it was.
	 */
	document.onkeyup = function(e) {
					// to have diagonal movement.
		var foo = document.getElementsByClassName('joystick');
		
	
	  var keyCode = (e.keyCode) ? e.keyCode : e.charCode;
	  if (KEY_CODES[keyCode]) {
		e.preventDefault();
			foo[0].classList.remove('right')
			foo[0].classList.remove('left')
		KEY_STATUS[KEY_CODES[keyCode]] = false;
	  }
	}
	//Agregar acción touch para ir a la izquierda
	document.getElementById("buttonA").addEventListener("touchstart", toLeft);
	function toLeft(evt) {
		evt.preventDefault();
		//Se setean variables para originar el movimiento
		KEY_STATUS[KEY_CODES[37]] = true;
		KEY_STATUS[KEY_CODES[39]] = false;
	}
	//Agregar acción touch para ir a la derecha
	document.getElementById("buttonB").addEventListener("touchstart", toRight);
	function toRight(evt) {
		evt.preventDefault();
		//Se setean variables para originar el movimiento
		KEY_STATUS[KEY_CODES[37]] = false;
		KEY_STATUS[KEY_CODES[39]] = true;
	}
	//Agregar acción touch para disparar
	document.getElementById("button").addEventListener("touchstart", myFire);
	function myFire(evt) {
		evt.preventDefault();
		//Se setean variable para originar acción de disparo
		KEY_STATUS[KEY_CODES[32]] = true;
	}
	//Agregar funciones para detener acción
	document.getElementById("buttonB").addEventListener("touchend", stopMove);
	document.getElementById("buttonA").addEventListener("touchend", stopMove);
	document.getElementById("button").addEventListener("touchend", stopMove);
	function stopMove(evt) {
		evt.preventDefault();
		//Se resetean variables para detener el movimiento
		KEY_STATUS[KEY_CODES[32]] = false;
		KEY_STATUS[KEY_CODES[37]] = false;
		KEY_STATUS[KEY_CODES[39]] = false;
	}
	
	/**
	 * requestAnim shim layer by Paul Irish
	 * Finds the first API that works to optimize the animation loop,
	 * otherwise defaults to setTimeout().
	 */
	window.requestAnimFrame = (function(){
		return  window.requestAnimationFrame       ||
				window.webkitRequestAnimationFrame ||
				window.mozRequestAnimationFrame    ||
				window.oRequestAnimationFrame      ||
				window.msRequestAnimationFrame     ||
				function(/* function */ callback, /* DOMElement */ element){
					window.setTimeout(callback, 1000 / 60);
				};
	})();
	
	
	
	
	var game = new Game();
	return {init:game.init,restart:game.restart, getScore:game.getScore, juegoTerminado:game.jT,setEndCallBack:game.setEndCallBack}
	
	}());
	
	
	
var app = angular.module("app", ["angularSpinner"]);

var config = {
  apiKey: "AIzaSyBuMdUCnKR6q9SLxsc0RdEKw9ReZilI1Bk",
  authDomain: "arcade-huawei.firebaseapp.com",
  databaseURL: "https://arcade-huawei.firebaseio.com",
  projectId: "arcade-huawei",
  storageBucket: "arcade-huawei.appspot.com",
  messagingSenderId: "370602322037"
};
firebase.initializeApp(config);

function obtenerScope() {
  return angular.element(document.body).scope();
}

function h9() {
  var $scope = obtenerScope("mainCtrl");
  $scope.gameOver = true;
  $scope.$apply();
}

/** c3 es la variable global que corresponde al juego */
var c4 = c3;

c4.setEndCallBack(window.h9);

app.run(function() {
  firebase
    .auth()
    .signInAnonymously()
    .catch(function(error) {
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
      // ...
    });
});

app.config([
  "usSpinnerConfigProvider",
  function(usSpinnerConfigProvider) {
    usSpinnerConfigProvider.setDefaults({ color: "red", size: 10 });
  }
]);

app.service("pageService", ['$rootScope', function($rootScope) {
  var srv = this;
  srv.pageSelected = "home";

  srv.selectPage = function(page) {
    srv.pageSelected = page;
    $rootScope.$broadcast("pageChanged", { page: page });
  };

  srv.checkPage = function(page) {
    return srv.pageSelected === page;
  };
}]);

app.service("usuarioService", ['$rootScope', function($rootScope) {
  var srv = this;
  srv.user = {};
  srv.refUsuarios = firebase.database().ref("/usuarios");
  srv.refScores = firebase.database().ref("/scores");

  /*SUBIR USUARIO*/
  srv.subirUsuario = function(nombre, correo, cedula) {
    srv.user.nombre = nombre;
    srv.user.correo = correo;
    srv.user.cedula = cedula;

    // /*
    //   * Register users in mixpanel
    // */
    // mixpanel.track("Users", {
    //   nombre: srv.user.nombre,
    //   correo: srv.user.correo,
    //   cedula: srv.user.cedula
    // });

    // mixpanel.people.set({
    //   $email: srv.user.correo,
    //   $last_login: new Date(),
    //   $name: srv.user.nombre
    // });

    // mixpanel.identify(srv.user.cedula);

    srv.refUsuarios
      .child("user" + cedula)
      .set({
        nombre: nombre,
        correo: correo,
        cedula: cedula,
        score: 0,
        fecha: String(new Date()),
        juegos: 0
      });
    srv.refScores
      .child("user" + cedula)
      .set({ score: 0, nombre: nombre, juegos: 0 });
  };

  srv.checkuser = function() {
    return srv.user && srv.user.cedula ? true : false;
  };
}]);

app.controller("puntajesController", ['pageService', '$scope', 'usSpinnerService', 'usuarioService', function(
  pageService,
  $scope,
  usSpinnerService,
  usuarioService
) {
  var ictrl = this;

  ictrl.puntajes;
  ictrl.pageService = pageService;
  ictrl.usuarioService = usuarioService;

  /*traer puntaje*/

  ictrl.traerNuevos = function() {
    usSpinnerService.spin("spinner-1");
    firebase
      .database()
      .ref("usuarios")
      .orderByChild("score")
      .limitToLast(10)
      .once("value", function(snapshot) {
        usSpinnerService.stop("spinner-1");
        //console.log('snap:', snapshot.val());
        ictrl.puntajes = snapshot.val();
        ictrl.topScores = Object.keys(ictrl.puntajes).map(function(key) {
          return ictrl.puntajes[key];
        });
        apply();
      });
  };

  ictrl.cambioPaginaHandler = function(event, args) {
    if (args.page && args.page == "scores") {
      console.log("entrando a scores");
      //console.log(ictrl);
      ictrl.topScores = [];
      ictrl.traerNuevos();
    }
  };

  $scope.$on("pageChanged", ictrl.cambioPaginaHandler);

  var apply = function() {
    if (
      $scope.$root &&
      $scope.$root.$$phase != "$apply" &&
      $scope.$root.$$phase != "$digest"
    )
      $scope.$apply();
  };
}]);

app.controller("mainCtrl", ['$timeout', '$scope', '$window', 'pageService', 'usuarioService', function(
  $timeout,
  $scope,
  $window,
  pageService,
  usuarioService
) {
  var apply = function() {
    if (
      $scope.$root &&
      $scope.$root.$$phase != "$apply" &&
      $scope.$root.$$phase != "$digest"
    )
      $scope.$apply();
  };

  var ctrl = this;
  ctrl.pageService = pageService;
  ctrl.usuarioService = usuarioService;
  ctrl.newGame = true;
  ctrl.lapppedInit;
  ctrl.usuarioRef;

  ctrl.user = {};

  $scope.puntajeActual = 0;

  $scope.gameOver = false;

  $scope.$watch("gameOver", function(newval, oldvar) {
    if (newval === true) {
      ctrl.guardar_score();
    }
  });

  firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
      var isAnonymous = user.isAnonymous;
      var uid = user.uid;
    } else {
    }
  });

  ctrl.saveUser = function() {
    console.log("jugardar usuari", ctrl.newGame);
    ctrl.usuarioService.subirUsuario(
      ctrl.user.nombre,
      ctrl.user.mail,
      ctrl.user.cedula
    );

    if (ctrl.newGame) {
      console.log("nuevo");
      ctrl.startgame();
      ctrl.newGame = false;
    } else {
      document.getElementById("loading").style.display = "none";
      ctrl.pageService.selectPage("");
      ctrl.restartgame();
    }
    document.getElementById("registerForm").reset();
  };
  ctrl.guardar_score = function() {
    // manejar envio de datos con firebase
    firebase
      .database()
      .ref("/scores")
      .child("user" + ctrl.user.cedula)
      .once("value", function(snap) {
        //Actualizando la cantidad de juegos totales
        var juegostotales = Number(snap.val().juegos)
          ? Number(snap.val().juegos) + 1
          : 1;
        firebase
          .database()
          .ref("/scores")
          .child("user" + ctrl.user.cedula)
          .update({ juegos: juegostotales });
        firebase
          .database()
          .ref("/usuarios")
          .child("user" + ctrl.user.cedula)
          .update({ juegos: juegostotales });

        //Actualizando el tiempo que le tomo el juego
        var finalizado = new Date().getTime();
        var total = (finalizado - ctrl.lapppedInit) / 1000;
        ctrl.partidaRef.update({ endTime: finalizado, totalTime: total, score: c4.getScore()});

        total = (finalizado - ctrl.lapppedInit) / 1000;

        //Actualizando el puntaje
        var score =
          snap.val() && snap.val().score != null ? snap.val().score : 0;
        if (score < parseInt(c4.getScore())) {
          firebase
            .database()
            .ref("/usuarios")
            .child("user" + ctrl.user.cedula)
            .update({
              score: parseInt(c4.getScore()),
              fecha: String(new Date()),
              tiempoTotal: total
            });
          firebase
            .database()
            .ref("/scores")
            .child("user" + ctrl.user.cedula)
            .update({
              score: parseInt(c4.getScore()),
              fecha: String(new Date()),
              tiempoTotal: total
            });
        }

        $scope.puntajeActual = parseInt(c4.getScore());
        ctrl.pageService.selectPage("gameOver");
        $timeout(function() {
          ctrl.pageService.selectPage("scores");
        }, 3000);

        document.getElementById("loading").style.display = "flex";
        apply();
      });

    $scope.gameOver = false;
  };

  function getCook(cookiename) {
    // Get name followed by anything except a semicolon
    var cookiestring = RegExp("" + cookiename + "[^;]+").exec(document.cookie);
    // Return everything after the equal sign, or an empty string if the cookie name not found
    return unescape(
      !!cookiestring ? cookiestring.toString().replace(/^[^=]+./, "") : ""
    );
  }

  //Sample usage
  /*Logica login*/
  ctrl.login = function() {
    srv = this;
    // console.log(ctrl.user.cedula);

    // /*
    //   * This tracks users activity 
    // */
    // mixpanel.track('Play game', {
    //   user: srv.user.cedula,
    //   nombre: srv.user.nombre
    // }
    // );

    // mixpanel.people.set({
    //   $last_login: new Date()
    // });

    // mixpanel.identify(srv.user.cedula);

    // /*
    // * Here ends mixpanel
    // */
    
    firebase
      .database()
      .ref("usuarios")
      .child("user" + ctrl.user.cedula)
      .once("value", function(snap) {
        ctrl.usuarioService.user = snap.val();
        ctrl.user = snap.val();

        if (snap.val() != null) {
          if (ctrl.newGame) {
            var cookieValue = getCook("instructions");
            if (cookieValue) {
              ctrl.startgame();
              ctrl.newGame = false;
            } else {
              document.cookie = "instructions=true";
              ctrl.pageService.selectPage("instructions");
            }
          } else {
            ctrl.restartgame();
          }
        } else {
          ctrl.pageService.selectPage("form");
        }
        apply();
      });
        document.getElementById("loginForm").reset();
  };

  ctrl.instructions = function() {
    c4.init();
    ctrl.newGame = false;
    console.log("go to game");
  };

  ctrl.prestartgame = function() {
    var usuarioRef = firebase
      .database()
      .ref("/usuarios/user" + ctrl.user.cedula);
    ctrl.lapppedInit = new Date().getTime();
    ctrl.partidaRef = usuarioRef
      .child("partidas")
      .push({ startTime: ctrl.lapppedInit });
  };

  ctrl.startgame = function() {
    ctrl.prestartgame();
    document.getElementById("loading").style.display = "none";
    //  ctrl.pageService.selectPage('');
    c4.init();
  };
  ctrl.restartgame = function() {
    ctrl.prestartgame();
    document.getElementById("loading").style.display = "none";
    ctrl.pageService.selectPage("");
    c4.restart();
  };

  $scope.isActive = false;
  ctrl.mute = function(event) {
    /*
    $scope.isActive = !$scope.isActive;
    if (c4.backgroundAudio.volume === 0.25) {
      c4.backgroundAudio.volume = 0;
      c4.gameOverAudio.volume = 0;
    } else {
      c4.backgroundAudio.volume = 0.25;
      c4.gameOverAudio.volume = 0.25;
    }
    */
  };
}]);

angular.bootstrap(document, ['app']);