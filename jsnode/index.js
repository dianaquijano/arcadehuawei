var admin = require("firebase-admin");
var express = require('express'), bodyParser = require('body-parser'), app = express()
admin.initializeApp({ credential: admin.credential.cert("arcadefep-firebase-adminsdk-weo3n-cea5d99aec.json"), databaseURL: "https://arcadefep.firebaseio.com" })
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

/**
 * Permite obtener los scores más altos.
 */
app.get('/best', function (req, res) {
  if (!req.query.arcade) 
    return res.status(404).end()
  
  admin.database().ref("scores").orderByChild("score").limitToLast(9).once('value', function (snap) {
      return res.json(snap.val())
  })

})

app.post('/save', function (req, res) {
  if (!req.body.arcade || !req.body.cedula) 
    return res.status(404).end()

  admin.database().ref("_pruebas").child().save();
    
})

app.listen(3000, function () {
  console.log('Example app listening on port 3000!\n')
})