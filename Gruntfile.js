'use strict';

module.exports = function(grunt) {
    // cargamos todas las tareas
    require('load-grunt-tasks')(grunt);
    grunt.loadNpmTasks('grunt-bower-concat');
    grunt.loadNpmTasks('grunt-cache-bust');

    // Configuracion del proyecto.
    grunt.initConfig({
        wiredep: {
            app: {
                src: ['index.html']
            }
        },
        bower_concat: {
            all: {
              dest: {
                'js': 'build/lib.js',
                'css': 'build/lib.css'
              },
              mainFiles: {
                'angular': 'angular.min.js',
              },            
              dependencies: {
                'angular-spinner': 'angular'
              },
            }
          },        
        includeSource: {
            dev: {
                files: {
                    //dest - src
                    'index.html': 'base/index.dev.html'
                }
            },
            dist: {
                files: {
                    //dest - src
                    'index.html': 'base/index.dev.html'
                }
            }
        },
        ngAnnotate: {
            options: {
                singleQuotes: true
            },
            app: {
                files: { '.tmp/js/app.js': ['trash.js','space_shooter.js','app.js', 'base/init-app.js'] }
            }
        },
        concat: {
            app: {
                src: ['.tmp/js/app.js'],
                dest: '.tmp/js/main.pack.js'
            },
            dev: {
                src: ['.tmp/js/app.js'],
                dest: 'build/js/main.js'
            }
        },
        uglify: {
            app: {
                src: ['.tmp/js/main.pack.js'],
                dest: 'build/js/main.js'
            }
        },
        obfuscator: {
            app: {
                files: {
                    'build/js/main.js': [ 'build/js/main.js' ]
                }
            }
        },
        clean: {
            tmp: '.tmp',
            build: 'build'
        },
        cacheBust: {
            taskName: {
                options: {
                    assets: ['build/**']
                },
                src: ['index.html']
            }
        }
    });

    // resgistrar las tareas
    grunt.registerTask('default', [
        'dev'
    ]);

    grunt.registerTask('dev', [
        'clean',
        'ngAnnotate',
        'concat:dev',
        'bower_concat',
        'includeSource:dev',
        'cacheBust'
    ]);

    grunt.registerTask('build', [
        'clean',
        'ngAnnotate',
        'concat:app',
        'uglify',
        'obfuscator',
        'bower_concat',
        'includeSource:dist',
        'clean:tmp',
        'cacheBust'
    ]);
};
